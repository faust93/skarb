Scarb::Application.routes.draw do
# resources :user

#root :to => 'application#index'
root :to => 'items#list'

match '/about' => 'application#about'

match 'basket/:id/add' => 'basket#add_item', :via=> [:get,:put], :as => :basket_add
match 'basket/:id/list' => 'basket#list', :via=> :get, :as => :basket_list
match 'basket/:id/delete' => 'basket#delete', :via=> :get, :as => :basket_delete

match 'inbox/:id/incoming' => 'inbox#incoming_list', :via=> :get, :as => :incoming_list
match 'inbox/:id/sent' => 'inbox#sent_list', :via=> :get, :as => :sent_list
match 'inbox/:id/notify' => 'inbox#notify_owner', :via=> :get, :as => :inbox_notify
match 'inbox/:id/reply' => 'inbox#reply', :via=> [:get,:post], :as => :inbox_reply
match 'inbox/:id/delete' => 'inbox#delete', :via=> :get, :as => :inbox_delete

match '/signup' => 'user#new'
match '/signin' => 'sessions#new', :via => [:get,:post], :as => :sessions_new
match '/signout' => 'sessions#destroy', :as => :sessions_out

match 'items' => 'items#list', :via=> :get, :as => :items_list
match 'items/create' => 'items#create', :via=> :post, :as => :items_create
match 'items/:id/update' => 'items#update', :via=> :put, :as => :items_update
match 'items/new' => 'items#new', :via=> [:get,:put], :as => :items_new
match 'items/:id' => 'items#show', :via=> :get, :as => :items
match 'items/:id/edit' => 'items#edit', :via=> [:get,:put], :as => :items_edit
match 'items/:id/delete' => 'items#delete', :via=> :get, :as => :items_delete

match 'user' => 'user#list', :via=> :get, :as => :user_list
match 'user/:id' => 'user#show', :via=> :get, :as => :user
match 'user/new' => 'user#new', :via=> :get, :as => :user_new
match 'user/create' => 'user#create', :via=> :post, :as => :user_create
match 'user/:id/edit' => 'user#edit', :via=> [:get,:put], :as => :user_edit
match 'user/:id/delete' => 'user#delete', :via=> :get, :as => :user_delete
match 'user/:id/items' => 'user#my_items', :via=> :get, :as => :user_items


get "user/update"

match 'category/:id' => 'category#show', :via=> :get, :as => :category
match 'category' => 'category#list', :via=> :get, :as => :category_list
 
  get "category/new"
 post "category/create"
  get "category/edit"
  get "category/update"
  get "category/delete"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #     match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'

end
