# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120702182404) do

  create_table "basket_items", :force => true do |t|
    t.integer "user_id"
    t.integer "item_id"
    t.integer "status"
  end

  create_table "categories", :force => true do |t|
    t.string   "category_name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "cities", :force => true do |t|
    t.string "city_name"
  end

  create_table "comments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.string   "comment"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "inbox_messages", :force => true do |t|
    t.integer  "user_id"
    t.integer  "msg_author_id"
    t.integer  "msg_type"
    t.text     "msg_body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "msg_status"
  end

  create_table "item_actions", :force => true do |t|
    t.string   "item_act"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "item_photos", :force => true do |t|
    t.integer  "item_id"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "items", :force => true do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.integer  "item_action_id"
    t.string   "item_name"
    t.text     "item_desc"
    t.string   "item_attr"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "simple_captcha_data", :force => true do |t|
    t.string   "key",        :limit => 40
    t.string   "value",      :limit => 6
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "simple_captcha_data", ["key"], :name => "idx_key"

  create_table "users", :force => true do |t|
    t.string   "user_name"
    t.string   "user_password"
    t.string   "user_email"
    t.string   "user_phone"
    t.integer  "user_city",          :limit => 255
    t.boolean  "user_show_personal"
    t.string   "user_attr"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "salt"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "users", ["user_email"], :name => "index_users_on_user_email", :unique => true

end
