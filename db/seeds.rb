# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

City.delete_all
open("/home/faust/ror_projects/scarb/db/city.txt") do |cities|
    cities.read.each_line do |city|
    City.create!(:city_name => city.chomp)
    end
end
