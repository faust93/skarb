class CreateInboxMessages < ActiveRecord::Migration
  def up
    create_table :inbox_messages do |t|
	t.column :user_id, :integer
	t.column :msg_author_id, :integer
	t.column :msg_type, :integer	# 0 inbox, 1 sent
	t.column :msg_body, :text
      t.timestamps
    end
  end
end
