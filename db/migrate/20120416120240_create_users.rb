class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
	t.column :user_name, :string
	t.column :user_password, :string
	t.column :user_email, :string
	t.column :user_phone, :string
	t.column :user_city, :integer
	t.column :user_show_personal, :boolean
	t.column :user_attr, :string

	t.column :photo_file_name, :string
	t.column :photo_content_type, :string
	t.column :photo_file_size, :integer
	t.column :photo_updated_at, :datetime
	
	t.column :salt, :string
	
      t.timestamps
    end
  end
end
