class CreateItemPhotos < ActiveRecord::Migration
  def up
    create_table :item_photos do |t|
	t.column :item_id, :integer
	t.column :photo_file_name, :string
	t.column :photo_content_type, :string
	t.column :photo_file_size, :integer
	t.column :photo_updated_at, :datetime
      t.timestamps
    end
  end
end
