class AddStatusToBasketItems < ActiveRecord::Migration
  def change
    add_column :basket_items, :status, :integer
  end
end
