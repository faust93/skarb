class AddEmailIndex < ActiveRecord::Migration
  def up
	add_index :users, :user_email, :unique => true
  end

  def down
  end
end
