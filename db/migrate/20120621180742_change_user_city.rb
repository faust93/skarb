class ChangeUserCity < ActiveRecord::Migration
  def up
	change_table :users do |t|
	    t.change :user_city, :integer
	end
  end

  def down
  end
end
