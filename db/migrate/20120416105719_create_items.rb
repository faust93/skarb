class CreateItems < ActiveRecord::Migration
  def up
    create_table :items do |t|
	t.column :user_id, :integer
	t.column :category_id, :integer
	t.column :item_action_id, :integer
	t.column :item_name, :string
	t.column :item_desc, :text
	t.column :item_attr, :string
      t.timestamps
    end
  end
end
