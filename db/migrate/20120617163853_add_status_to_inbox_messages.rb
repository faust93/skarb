class AddStatusToInboxMessages < ActiveRecord::Migration
  def change
    add_column :inbox_messages, :msg_status, :integer
  end
end
