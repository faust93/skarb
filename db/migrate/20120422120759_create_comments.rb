class CreateComments < ActiveRecord::Migration
  def up
    create_table :comments do |t|
    t.column :user_id, :integer
    t.column :item_id, :integer
    t.column :comment, :string
      t.timestamps
    end
  end
end
