class CreateCategories < ActiveRecord::Migration
  def up
    create_table :categories do |t|
	t.column :category_name, :string
      t.timestamps
    end
  end
end
