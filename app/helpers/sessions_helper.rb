module SessionsHelper

 def session_expiry
    if signed_in?
     @time_left = (session[:expires_at] - Time.now).to_i
     unless @time_left > 0
      sign_out
      flash[:error] = t(:session_expired)
      redirect_to items_list_path
     end
    end
 end

 def update_activity_time
    if signed_in?
     session[:expires_at] = 30.minutes.from_now
    end
 end

    def sign_out
     session[:user_id] = nil
     session[:user_type] = nil
     session[:expires_at] = nil
    end

    def sign_in(user)
        session[:expires_at] = 30.minutes.from_now
        session[:user_id] = user.id
	if user.user_name == "faust93"
	    session[:user_type] = :admin
	else
	    session[:user_type] = :user
	end
    end

    def signed_in?
	!current_user.nil?
    end
    
    def is_admin?
	if session[:user_type] == :admin
	    return true
	else
	    return false
	end
    end
      
##private

    def current_user
      @current_user ||= User.find_by_id(session[:user_id]) if session[:user_id]
    end

    def require_login
    unless current_user
          flash[:error] = "You must b logged in to access this section"
          render :file => 'sessions/login_warn'
    end
end

end
