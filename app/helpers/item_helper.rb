module ItemHelper

 def latest_items
     Item.order("created_at DESC").limit(3)
 end

end
