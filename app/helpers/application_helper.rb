module ApplicationHelper

def title
        base_title = "Skarb"
        if @title.nil?
          base_title
          else
        "#{base_title} | #{@title}"
	end
end
    
def  nav_link(name, controller, action, url)
    html = ""
    if action.nil?
    	if params[:controller] == controller
	    html << "<li class='current_page_item'> #{link_to(name, url)}</li>"
	else
	    html << "<li>#{link_to(name, url)}</li>"
	end
    else
    	if params[:controller] == controller && params[:action] == action
	    html << "<li class='current_page_item'> #{link_to(name, url)}</li>"
	else
	    html << "<li>#{link_to(name, url)}</li>"
	end
    end    
    html
end
          
def errors_for(object, message=nil)
    html = ""
    unless object.errors.blank?
#     html << "<div class='formErrors #{object.class.name.humanize.downcase}Errors'>\n"
     html << "<div id='error_box'>\n"

	if message.blank?
            if object.new_record?
                      html << "\t\t<h5>There was a problem creating the #{object.class.name.humanize.downcase}</h5>\n"
                else
                    html << "\t\t<h5>There was a problem updating the #{object.class.name.humanize.downcase}</h5>\n"
                end    
         else
            html << "<h3 class=\"fb lp10\">#{message}</h3>"
        end  
      html << "\t\t<ul>\n"
    
        object.errors.full_messages.each do |error|
            html << "\t\t\t<li class=\"c00\">#{error}</li>\n"
        end
    
      html << "\t\t</ul>\n"
      html << "\t</div>\n"
    end
      html
    end

                                                
end
