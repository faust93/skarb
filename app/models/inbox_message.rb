class InboxMessage < ActiveRecord::Base
  belongs_to :user
  attr_accessible :msg_author_id, :msg_type, :msg_body
end
