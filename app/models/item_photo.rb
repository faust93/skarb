require 'paperclip'

class ItemPhoto < ActiveRecord::Base
    belongs_to :item
    has_attached_file :photo, :styles => { :small => "140x140>", :large => "800x600>" }
    validates_attachment_size :photo, :less_than => 1.megabytes
    attr_accessible :photo
end
