require 'digest'
class User < ActiveRecord::Base
    # generate password accessor
    apply_simple_captcha
    
    attr_accessor :password
    
    attr_accessible :captcha, :captcha_key, :password, :password_confirmation, :photo, :user_name, :user_password, :user_email, :user_city, :user_phone, :user_show_personal, :user_attr

    has_many :basket_items, :dependent => :destroy	# user's basket 
    has_many :inbox_messages, :dependent => :destroy	#  inbox: incomind/sent messages
    has_many :items, :dependent => :destroy		# user items
    has_many :comments, :dependent => :destroy

    accepts_nested_attributes_for :inbox_messages, :basket_items, :items, :comments
    
    has_attached_file :photo, :styles => { :small => "150x180>", :large => "250x280>" }
    validates_attachment_size :photo, :less_than => 1.megabytes
    
    validates :user_name, :length => { :maximum => 32, :minimum => 4 },
	      :uniqueness => { :case_sensitive => false }
    validates :user_email,
	      :format => { :with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i },
	      :uniqueness => { :case_sensitive => false }
    validates :password, :confirmation => true, :length => { :within => 6..40 }

    before_save :encrypt_password
    
    def city
	City.find_by_id(self.user_city).city_name
    end
    
    def has_password?(submitted_password)
     self.user_password == encrypt(submitted_password)
    end
    
    def self.authenticate(email, submitted_password)
	user = find_by_user_email(email)
        return nil  if user.nil?
        return user if user.has_password?(submitted_password)
    end          

    
private

    def encrypt_password
	self.salt = make_salt if new_record?
	self.user_password = encrypt(self.password)
    end
              
    def make_salt
        secure_hash("#{Time.now.utc}--#{password}")
    end
                        
    def secure_hash(string)
        Digest::SHA2.hexdigest(string)
    end
                                      
    def encrypt(string)
	secure_hash("#{salt}--#{string}")
    end
    
end
