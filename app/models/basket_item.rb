class BasketItem < ActiveRecord::Base
   belongs_to :user
   attr_accessible :user_id, :item_id, :status
end