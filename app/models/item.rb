class Item < ActiveRecord::Base
    belongs_to :category
    belongs_to :item_action
    belongs_to :user

    has_many :item_photos, :dependent => :destroy
    has_many :comments, :dependent => :destroy

    accepts_nested_attributes_for :comments, :item_photos, :allow_destroy => true

    validates :item_name, :presence => true, :length => { :maximum => 128, :minimum => 3 }
    validates :item_desc, :presence => true, :length => { :maximum => 1024, :minimum => 20 }

    attr_accessible :category_id, :item_action_id, :item_name, :item_desc, :item_action, :item_attr, :item_photos, :item_photos_attributes
end

