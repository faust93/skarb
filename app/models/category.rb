class Category < ActiveRecord::Base
    has_many :items
    validates_presence_of :category_name, :on => :create, :message => "can't be blank"
    attr_accessible :category_name
    accepts_nested_attributes_for :items
end
