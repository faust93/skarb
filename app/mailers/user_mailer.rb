class UserMailer < ActionMailer::Base
  default from: "c0t0d0s3@gmail.com"

def welcome_email(user)
    @user = user
    @url = "http://xep.8800.org:4040"
    mail(:to => user.user_email, :subject => "Welcome to SKARB")
    end
    
def notify_email(user_from, user_to, item)
    @user_from = user_from
    @user_to = user_to
    @item = item
    mail(:to => user_to.user_email, :subject => "SKARB new message notification")
    end
end
