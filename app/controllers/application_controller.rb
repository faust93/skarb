class ApplicationController < ActionController::Base
  protect_from_forgery

    include SessionsHelper

before_filter :set_locale

before_filter :session_expiry, :except => [:sessions_new, :sessions_out]
before_filter :update_activity_time, :except => [:sessions_new, :sessions_out]

  def set_locale
      I18n.locale = params[:locale] || I18n.default_locale
    end
        
    def index
#    render(:text => "Hey - Rails is cool!")
#           redirect_to(items_list_path)
    end

end
