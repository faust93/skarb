class CategoryController < ApplicationController
 before_filter :require_login

  def list
    @categories = Category.find(:all)
  end

  def show
    @category = Category.find(params[:id])
    @items = Item.where("category_id=?", params[:id] )
  end

  def new
#    @category = Category.new
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      redirect_to :action => 'list'
     end
  end

  def edit
  end

  def update
  end

  def delete
  end
end
