class ItemsController < ApplicationController
 before_filter :require_login, :only => [:new, :edit, :delete, :create, :update]
  
  def new
    @item = Item.new
    3.times {@item.item_photos.build}
  end

# edit item
  def edit

    if request.put?
     @item = Item.find(params[:id])
    if is_admin? || @item.user_id == current_user.id

	if !params[:pix_destroy].blank?
	     params[:pix_destroy].each { |pix|
    		@item.item_photos[pix.to_i].delete
	    }
        end
	    if @item.update_attributes(params[:item])
	    redirect_to :controller => 'user', :action => 'my_items', :id => current_user
		else
    		flash.now[:error] = "Can't update item!"
	    end
     else
     flash[:error] = "It is not your item!"
     redirect_to :action=>'list'
     end

    else

     @item = Item.find_by_id(params[:id])
     if is_admin? || @item.user_id == current_user.id
     for pix in (0..2)
      @item.item_photos.build if @item.item_photos[pix].blank? 
     end
     else
     flash[:error] = "It is not your item!"
     redirect_to :action=>'list'
     end

    end
  end

# list items 
  def list
	where_str = ""
	tmp_cid = params[:category]
	if !tmp_cid.blank?
	    where_str = "category_id = #{tmp_cid}"
	end
	tmp_aid = params[:iaction]
	if !tmp_aid.blank?
	    if where_str.blank?
	    where_str += "item_action_id = #{tmp_aid}"
	    else
	    where_str += " AND item_action_id = #{tmp_aid}"
	    end
	end
	case params[:sort]
	    when "0"
		order_str = "created_at DESC"
	    when "1"
		order_str = "item_name"
	    when "2"
		order_str = "user_id"
	    when "3"
		@items = Item.joins(:user).order("user_city").page(params[:page]).per(2)
		return
	    else
		order_str = "created_at DESC"
	    end
        @items = Item.where(where_str).order(order_str).page(params[:page]).per(2)
  end

 def delete
    @item = Item.find_by_id(params[:id])
    if is_admin? || @item.user_id == current_user.id
    if @item.destroy
    	redirect_to :action => 'list'
    else
	redirect_to :action => 'delete'
    end
    else
    flash[:error] = "It is not your item!"
    redirect_to :action=>'list'
    end
 end

def show
    @title = "Show Item"
    @item = Item.find(params[:id])
    @category = Category.find(@item.category_id)
    @action = ItemAction.find(@item.item_action_id)
    @user = User.find(@item.user_id)
end


  def create
#    @name = params[:user]
#    render(:text => "Hey - Rails is cool! #{@name['user_name']}")

        @item = Item.new(params[:item])
        @item.user_id = current_user.id		# future: check err
	if @item.save
		redirect_to :action => 'list'
	else
            3.times {@item.item_photos.build} if @item.item_photos.blank?
	    render :action => 'new'
        end
  end

 def update
 end

end
