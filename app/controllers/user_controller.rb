class UserController < ApplicationController
 before_filter :require_login, :only => [:show, :list, :my_items, :edit, :delete]
 
def list
	@title = t(:users)
        @users = User.find(:all)
end

def my_items
	@title = t(:my_items)
        @items = Item.where("user_id=?", params[:id] ).order('created_at').page(params[:page]).per(10)
#        @items = User.find(params[:id]).items.find_all.order('created_at').page(params[:page]).per(10)
end

def edit
    @title = t(:profile_edit)
    if request.put?
      @user = User.find(params[:id])
	if @user.update_attributes(params[:user])
	redirect_to :action => 'show', :id => @user
	    else
    	    flash.now[:error] = "Can't update user attributes!"
	end
    else
    	if is_admin?
	  @user = User.find(params[:id])
	else
	  @user = User.find(session[:user_id])
	end
    end
end

    def update
    end

     
  def show
    @user = User.find(params[:id])
    @title = @user.user_name
  end

  def new
    @user = User.new
  end

 def delete
    @user = User.find(params[:id])
    if @user.destroy
    	redirect_to :action => 'list'
    else
	redirect_to :action => 'delete'
    end
 end
   
  def create
    @user = User.new(params[:user])
#    @user.save_with_captcha
    if @user.save_with_captcha
	flash[:success] = t(:welcome)

	UserMailer.welcome_email(@user).deliver
	sign_in(@user)
	redirect_to @user

    else
	flash[:error] = t(:user_error)
	render :action => 'new'
     end
  end

end
