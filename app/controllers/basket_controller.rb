class BasketController < ApplicationController
 before_filter :require_login

    def add_item
	if !current_user.basket_items.create(:item_id => params[:id], :status => ITEM_NEW)
	    flash[:error] = t(:basket_error)
	end
	redirect_to request.referer
    end

    def list
	if is_admin? || params[:id] == current_user.id
	 basket = User.find(params[:id]).basket_items
	else
	 basket = User.find(current_user.id).basket_items
	end
	@user_basket = Kaminari.paginate_array(basket).page(params[:page]).per(10)
    end

    def delete
	@basket_item = current_user.basket_items.find_by_id(params[:id])
	if !@basket_item.nil?
         @basket_item.destroy
	end
     redirect_to :action => 'list', :id => current_user.id
    end
end
