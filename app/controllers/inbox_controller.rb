# encoding: UTF-8

class InboxController < ApplicationController
before_filter :require_login

# list incoming messages
    def incoming_list
        if is_admin? || params[:id] == current_user.id
	 msgs = User.find_by_id(params[:id]).inbox_messages.order("created_at DESC").find_all_by_msg_type(INBOX)
	else
	 msgs = User.find_by_id(current_user.id).inbox_messages.order("created_at DESC").find_all_by_msg_type(INBOX)
	end
	@messages = Kaminari.paginate_array(msgs).page(params[:page]).per(10)
    end

# list sent messages
    def sent_list
        if is_admin? || params[:id] == current_user.id
	 msgs = User.find_by_id(params[:id]).inbox_messages.find_all_by_msg_type(SENT)
	else
	 msgs = User.find_by_id(current_user.id).inbox_messages.find_all_by_msg_type(SENT)
	end
	@messages = Kaminari.paginate_array(msgs).page(params[:page]).per(10)
    end

# create notification message in item's owner inbox
    def notify_owner
	item = Item.find_by_id(params[:id])
	if item.nil?
	flash[:error] = t(:missing_item)
	redirect_to :controller => 'basket', :action => 'list', :id => current_user
	end
	# update in basket item status to notified
	b_item = current_user.basket_items.find_by_item_id(params[:id])
	b_item.update_attribute(:status, ITEM_NOTIFYED)
    
	message = InboxMessage.new
	# mesg destination - item's owner INBOX
	message.user_id = item.user_id
	message.msg_type = INBOX
	message.msg_status = MSG_NEW
	message.msg_author_id = current_user.id
	message.msg_body = "Пользователь #{current_user.user_name} проявил интерес к " + '<a href="'+url_for(:controller => 'items', :action =>'show', :id => item.id)+'">'+"#{item.item_name}</a>"
	if message.save
		UserMailer.notify_email(current_user, User.find_by_id(item.user_id), item.item_name).deliver
		flash[:notice] = t(:notify_wait)
		redirect_to :controller => 'basket', :action => 'list', :id => current_user
	else
	    render :action => 'new'
        end

    end

# delete msg
    def delete
	@msg = InboxMessage.find_by_id(params[:id])
    if is_admin? || @msg.user_id == current_user.id
	if @msg.destroy
	redirect_to request.referer
#    	redirect_to :action => 'incoming_list', :id => current_user.id
	else
        flash[:error] = t('msg_delete_error')
	redirect_to request.referer
#	redirect_to :action => 'delete'
	end
    else
    	redirect_to :action => 'incoming_list', :id => current_user.id
    end
    end

 def reply
    if request.post?
	msg = InboxMessage.new
	msg.user_id = InboxMessage.find_by_id(params[:id]).msg_author_id
	msg.msg_type = INBOX
	msg.msg_status = MSG_NEW
	msg.msg_author_id = current_user.id
	msg.msg_body = params[:reply][:msg_body]
	
	# copy message to sent box
	msg_sent = msg.dup
	msg_sent.msg_type = SENT
	msg_sent.user_id = current_user.id
	msg_sent.msg_author_id = InboxMessage.find_by_id(params[:id]).msg_author_id
	
	if msg.save && msg_sent.save
	    redirect_to :action => 'incoming_list', :id => current_user.id
	else
	    flash[:error] = t('msg_save_error')
	    redirect_to :action => 'reply'
        end
    else
    @title = "Reply"
    @msg = InboxMessage.find_by_id(params[:id])
    if !@msg.nil?
        if @msg.user_id != current_user.id
	flash[:error] = "It is not your message!"
	redirect_to :action => 'reply'
	return
	end	
    else
	flash[:error] = "Can't find message!"
	redirect_to :action => 'incoming_list', :id => current_user.id
	return
    end
    
    end
 end

end
