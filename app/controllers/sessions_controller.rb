class SessionsController < ApplicationController

def new
    if request.post?
	user = User.authenticate(params[:session][:email], params[:session][:password])
	if user.nil?
    	    flash.now[:error] = t(:invalid_login)
            @title = "Sign in"
            render :action => 'error'
	else
	    flash[:success] = "Welcome #{user.user_name}!"
            @title = "Logged in!"
	    sign_in(user)
            redirect_to(user)
	end
    else
    @title = "Sign in"
    end
end
      
def create
end
          
def destroy
    session[:user_id] = nil
    session[:user_type] = nil
    redirect_to(root_url)
end

end
