<h1>Add new item</h1>
<%= form_for @item, :url => items_create_path, :html => {:multipart => true} do |f| %>
<p>
<%= f.label :item_name %>
<%= f.text_field :item_name %>
</p>

<p><label for="item_desc">Item description</label>:</p>
<p>
<%= f.text_area :item_desc, :size => "60x12" %>
</p>
<p><%= collection_select :item, :category_id, Category.find(:all), :id, :category_name %></p>
<p><%= collection_select :item, :item_action_id, ItemAction.find(:all), :id, :item_act %></p>
<p><label for="photos">Item photos</label>:</p>
<%= f.fields_for :item_photos do |builder| %>
<% if builder.object.new_record? %>
<p>
<%= builder.label :photo, "Image File" %>
<%= builder.file_field :photo %>
</p>
<% end %>
<% end %>
<%= f.submit "Create" %>
<% end %>

