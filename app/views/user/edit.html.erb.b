<h1>123</h1>
<%= form_for @user, :url => user_edit_path(@user), :html => {:multipart => true} do |f| %>

<%= errors_for(@user, "error".html_safe %>

<%= f.label :user_name %><br>
<%= f.text_field :user_name %><br>

<%= f.label :user_email %><br />
<%= f.text_field :user_email %><br>

<%= f.label :password %><br />
<%= f.password_field :password %><br>

<%= f.label :password_confirmation, "Confirmation" %><br />
<%= f.password_field :password_confirmation %><br>

<%= f.label :user_phone %><br />
<%= f.text_field :user_phone %><br>

<%= f.label :user_city %><br />
<%= collection_select :user, :user_city, City.find(:all), :id, :city_name %>

<p><label for="photo">User photo</label>:</p>
<%= image_tag @user.photo.url(:small) %><br>

<%= f.file_field :photo %></p>
<p><%= f.check_box :user_show_personal %> Show personal data</p>
<%= f.submit "Save changes" %>
<% end %>
<%= link_to 'Back', request.referer %>
